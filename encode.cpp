#include "encode.hpp"

BitReader BitReader::open(const std::vector <std::uint8_t> &data) {
    return BitReader{ data };
}

uint64_t BitReader::read_raw(uint64_t size) {
    assert(size <= 64);
    if (data.empty()) throw std::runtime_error("Attempted to read from empty BitReader");
    auto ret = 0ull;
    auto left = size;
    while (left > 0) {
        if (cursor.byte > data.size() - 1)
            throw std::runtime_error("Expected more data but none remains to be read");

        auto x = std::min(left, 8 - cursor.bit_offset);
        auto mask = ~(~0ull << x);
        auto byte = data[cursor.byte];
        ret |= ((uint64_t)(byte >> cursor.bit_offset) & mask) << (size - left);
        cursor.bit_offset += x;
        left -= x;

        if (cursor.bit_offset == 8) {
            cursor.byte += 1;
            cursor.bit_offset = 0;
        }
    }
    return ret;
}

void BitWriter::write_raw(uint64_t size, uint64_t v) {
    assert(size <= 64);
    auto left = size;
    while (left > 0) {
        if (data.size() != cursor.byte + 1) {
            data.push_back(0);
        }
        auto x = std::min(left, 8 - cursor.bit_offset);
        auto byte = data[cursor.byte];
        auto mask = ~(~0ull << x);
        byte |= (uint8_t)(((v >> (size - left)) & mask) << cursor.bit_offset);
        data[cursor.byte] = byte;
        cursor.bit_offset += x;
        left -= x;
        if (cursor.bit_offset == 8) {
            cursor.byte += 1;
            cursor.bit_offset = 0;
        }
    }
}

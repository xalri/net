//! Sequences game events.
//!
//! An event is a discrete change to the game world. Events are ordered, and
//! a sequence of events describe the progression of a world over time.
//! Events include things like players joining, chat messages being sent,
//! a plane using an ability, and so on.
//!
//! Contiguous blocks of events are grouped together into event groups where each
//! event group is (usually) the events created by some source in one tick
//! of the game loop. Event groups have a number, to keep them in order, their sequence
//! number, or seq for short. When clients and servers share event groups they attach
//! the seq to each event group.
//!
//! Each packet is made up of a sequence number for the packet itself, zero to
//! three event groups, and metadata for resending missing events.
//! For each message, the server adds all the new event groups to it's buffer
//! in their raw (non-decoded) form.
//! For each event group, events are read until there is no data left, and applied
//! to the simulation as they're decoded.

#pragma once

#include <cstdint>
#include <vector>
#include <deque>
#include <optional>
#include <map>
#include <iostream>
#include "xal_buffer.h"
#include "seq.hpp"

/// A group of game events.
struct EvGroup {
    /// This event group's sequence number
    SeqSmall seq{};
    NestedData data{};

    using rule = all_of<
       field_rule<&EvGroup::seq, SeqSmall::rule>,
       field_rule<&EvGroup::data, NestedData::rule>
    >;
};

/// A packet of discrete sequenced events.
struct SequentialData {
    /// The global game counter, reset when the map changes. A server should
    /// discard any messages with a counter who's last bit doesn't match it's own.
    /// Only the least-significant bit is encoded, enough to distinguish
    /// between packets from the current map and previous map for the few seconds
    /// after a map change.
    uint8_t counter = 0;

    /// The sequencing number of this packet.
    SeqSmall seq {};

    /// The most recent event group sequencing number.
    SeqSmall group_seq;

    /// Groups of events (at most 4 groups per packet)
    std::vector<EvGroup> ev_groups;

    /// A list of sequence numbers of missing messages (at most 4 IDs per packet)
    std::vector<SeqSmall> missing;

    using rule = all_of<
        field_rule<&SequentialData::counter, bits_rule<uint8_t, 1>>,
        field_rule<&SequentialData::seq, SeqSmall::rule>,
        field_rule<&SequentialData::group_seq, SeqSmall::rule>,
        field_rule<&SequentialData::ev_groups, vec_rule<EvGroup::rule, 3>>,
        field_rule<&SequentialData::missing, vec_rule<SeqSmall::rule, 3>>
    >;
};

/// Metadata about some missing event group, used to decide which missing groups to request in each
/// packet.
struct Missing {
    Seq msg = { 0 };
    Seq group = { 0 };
    uint64_t resend_reqs = 0;
    uint64_t next_req = 0;

    bool send() {
        if (next_req == 0) {
            resend_reqs += 1;
            next_req = resend_reqs;
            return true;
        } else {
            next_req -= 1;
            return false;
        }
    }
};

/// A sequencer for the data in game messages
template<class EvIn, class EvOut>
struct Sequencer {
    // True while the connection is open
    bool alive = true;
    // An immutable value which identifies which game this sequencer's
    // events belong to, any messages with an un-matching counter are ignored.
    uint8_t counter = 0;
    // A buffer of all the event groups we've constructed
    Buffer<EvGroup, 810> out_buf {};
    // A buffer of the event group IDs we sent in each message
    Buffer<std::vector<Seq>, 270> out_msg_buf {};
    // The IDs of any unsent event groups
    std::deque<Seq> out_ids {};
    // The set of event groups we need to resend
    std::vector<Seq> out_resend {};
    // Builds the current event group
    BitWriter out_builder {};
    // The sequence of the next message
    Seq out_msg_seq = { 0 };
    // The sequencing number of the event group we're building
    Seq out_group_seq = { 0 };

    std::vector<Missing> missing {};
    std::vector<Seq> missing_groups {};
    Seq last_group_missing = { -1 };

    // The event group we're dispatching and the current position in bits
    std::optional<std::pair<EvGroup, size_t>> dispatch {};
    // A queue of event groups ready to be dispatched
    std::deque<EvGroup> dispatch_buffer {};
    // The sequence number of the last message we received
    Seq in_seq = Seq{ -1 };
    // The sequence number of the sender's most recently created event group.
    // This isn't necessarily the same as their most recently sent, since at
    // most 3 event groups fit into a single message.
    Seq in_group_seq = Seq{ -1 };
    // The sequence number of the event group we most recently added to the
    // dispatch queue
    Seq in_dispatched = Seq{ -1 };
    // An order buffer for incoming event groups
    std::map<int64_t, EvGroup> in_buffer {};

    /// Get the group ID of the event group currently being dispatched
    Seq dispatch_group_id() {
        if (dispatch.has_value()) return expand(dispatch->first.seq, in_dispatched);
        else return Seq { -1 };
    }

    /// Decode the next incoming event.
    template <class R>
    std::optional<EvIn> next_ev(R rule) {
        if (dispatch.has_value()) {
            auto& pos = dispatch->second;
            auto const& group = dispatch->first;

            if (pos < group.data.len) {
                auto reader = BitReader::open(group.data.data);
                reader.seek(pos);

                if (pos == 0) {
                    auto is_fragment = false;
                    bool_rule{}.read(reader, is_fragment);

                    if (is_fragment) {
                        std::cerr << "fragmented event groups unimplemented\n";
                        exit(1);
                    }
                }

                try {
                    EvIn ev;
                    rule.read(reader, ev);
                    pos = reader.pos();
                    return { ev };
                } catch(std::exception &e) {
                    // TODO: this probably invalidates the world state, should we just disconnect?
                    std::cerr << "Failed to read incoming event, ignoring group: " << e.what() << "\n";
                }
            }
        }
        dispatch = {};

        // The block above returns if we have read the next event, and continues
        // to here if we've either finished reading the current group or we
        // got an error trying to read an event, so next we attempt to call
        // ourselves again with the next group, returning `None` if there are no
        // more groups on the dispatch queue.
        if (dispatch_buffer.empty()) return {};

        dispatch = {{ dispatch_buffer.front(), 0 }};
        dispatch_buffer.pop_front();

        return next_ev(rule);
    }

    /// Handle an incoming packet, returns true if we have dispatched all the
    /// event groups the sender has sent (meaning it's safe to read transient
    /// entity data as soon as the new events have been processed)
    bool receive(SequentialData& data) {
        if (data.counter != counter) return false;

        auto msg_seq = expand(data.seq, in_seq);
        auto group_seq = expand(data.group_seq, in_group_seq);

        if (group_seq.seq > in_dispatched.seq + 1) {
            for(auto i = in_seq.seq + 1; i < msg_seq.seq; i++) {
                missing.push_back({ Seq{ i }, group_seq });
            }
        }

        if (msg_seq > in_seq) in_seq = msg_seq;
        if (group_seq > in_group_seq) in_group_seq = group_seq;

        for (auto group : data.ev_groups) {
            auto seq = expand(group.seq, group_seq);
            auto is_new = seq > last_group_missing;
            auto missing_it = std::find(missing_groups.begin(), missing_groups.end(), seq);
            auto was_missing = missing_it != missing_groups.end();

            if (is_new) {
                for (auto i = last_group_missing.seq + 1; i < seq.seq; i++) {
                    missing_groups.push_back(Seq{ i });
                }
                last_group_missing = seq;
            } else if (was_missing) {
                missing_groups.erase(missing_it);
            }

            if (is_new || was_missing) {
                if (seq.seq == in_dispatched.seq + 1) {
                    in_dispatched = seq;
                    dispatch_buffer.push_back(group);
                } else {
                    in_buffer.insert({seq.seq, std::move(group)});
                }

                auto i = in_dispatched.seq + 1;
                decltype(in_buffer.begin()) it;
                while ((it = in_buffer.find(i)) != in_buffer.end()) {
                    dispatch_buffer.push_back(it->second);
                    in_dispatched.seq = i;
                    in_buffer.erase(it);
                }
            }
        }

        for (auto s : data.missing) {
            auto seq = expand(s, out_msg_seq);
            std::cerr << "Got negative ack, truncated_seq: " << seq.seq << ", full_seq: " << seq.seq << "\n";

            // Find the closest message which contained some new events and
            // add those events to the resend queue.
            auto i = (uint64_t) seq.seq;
            while (true) {
                std::vector<Seq> const* groups = out_msg_buf.get(i);
                if (!groups) {
                    std::cerr << "Negative ack out of window, current seq " << out_msg_seq.seq << " :<\n";
                    alive = false;
                    return false;
                }

                if (groups->empty()) {
                    i += 1;
                    continue;
                }

                for (auto& x : *groups) out_resend.push_back(x);
                std::sort(out_resend.begin(), out_resend.end());
                out_resend.erase(std::unique(out_resend.begin(), out_resend.end()), out_resend.end());
            }
        }

        return in_dispatched >= group_seq;
    }

    /// Build the current event group, adding it to the buffer and incrementing
    /// the event group sequence number
    void next_group() {
        auto group = EvGroup { truncate(out_group_seq), NestedData::from_writer(out_builder) };
        out_builder = BitWriter {};

        out_buf.add(group);
        out_ids.push_back(out_group_seq);
        out_group_seq.seq += 1;
    };

    /// Add a new event to be sent in the next message
    template<class Rule>
    void add_event(Rule rule, EvOut const& ev) {
        // Get the size of the encoded event
        auto writer = BitWriter{};
        rule.write(writer, ev);
        auto size = writer.pos();

        if (size > 2047) {
            throw std::runtime_error("Event too large"); // TODO: ev group fragmentation...
        }

        // If the current event group is too large to add this event, create a
        // new group and add the current one to the buffer
        if (out_builder.pos() + size > 2047) {
            next_group();
        };

        if (out_builder.pos() == 0) {
            // For the to-be-implemented event group fragmentation:
            // Each group starts with a bool, true if it's split into multiple messages.
            bool_rule{}.write(out_builder, false);
        }

        rule.write(out_builder, ev);
    }

    SequentialData make_msg() {
        if (out_builder.pos() != 0) next_group();

        // Add any event groups we need to resend
        while (out_ids.size() < 3 && !out_resend.empty()) {
            out_ids.push_front(out_resend.back());
            out_resend.pop_back();
        }

        // The event groups to send in this message
        auto ev_groups = std::vector<EvGroup>{};
        auto group_seqs = std::vector<Seq>{}; // The sequence numbers of the included event groups

        while (ev_groups.size() < 3 && !out_ids.empty()) {
            auto seq = out_ids.front();
            out_ids.pop_front();

            auto group = out_buf.get((size_t) seq.seq);
            if (!group) {
                std::cerr << "Can't satisfy resend request, not in buffer (group id " << seq.seq << ")\n";
            } else {
                ev_groups.push_back(*group);
                group_seqs.push_back(seq);
            }
        }
        // Store the ev group IDs so we can resend them if the message is lost.
        out_msg_buf.add(group_seqs);

        // Request that the client resend the events which were in messages
        // we haven't received.
        auto _missing = std::vector<SeqSmall>{};

        auto i = 0;
        while (i < missing.size() && _missing.size() <= 4) {
            if (missing[i].group <= in_dispatched) {
                missing.erase(missing.begin() + i);
                continue;
            }
            if (missing[i].send()) {
                _missing.push_back(truncate(missing[i].msg));
            }
            i++;
        }

        auto group_seq = truncate(Seq{ out_group_seq.seq - 1 });
        auto data = SequentialData{ counter, truncate(out_msg_seq), group_seq, ev_groups, _missing };
        out_msg_seq.seq += 1;
        return data;
    };
};


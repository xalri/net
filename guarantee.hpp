//! Vocabulary of types used in the guarantee system.

#pragma once

#include <iostream>
#include "encode.hpp"
#include "seq.hpp"
#include "zed_net.h"

struct OpenData {
    int32_t id {};
    bool is_open {};
    SeqSmall seq {};

    using rule = all_of<
            field_rule<&OpenData::id, i32_rule>,
            field_rule<&OpenData::is_open, bool_rule>,
            field_rule<&OpenData::seq, SeqSmall::rule>
    >;
};

struct Header {
    enum class Kind { Open, OpenAck, Data, DataAck, Count };
    using kind_rule = enum_rule<(uint64_t) Kind::Count>;

    Kind kind = Kind::Open;

    union {
        OpenData open {};
        int32_t open_ack;
        SeqSmall data;
        SeqSmall data_ack;
    };

    using rule = variant_rule<
            field_rule<&Header::kind, kind_rule>,
            field_rule<&Header::open, OpenData::rule>,
            field_rule<&Header::open_ack, i32_rule>,
            field_rule<&Header::data, SeqSmall::rule>,
            field_rule<&Header::data_ack, SeqSmall::rule>
    >;
};

enum class GuaranteeLevel { None, Delivery, Order, Ping, Count };

using guarantee_level_rule = enum_rule<(uint64_t) GuaranteeLevel::Count>;

struct SocketAddr: public zed_net_address_t {
    bool operator==(SocketAddr const& rhs) {
        return host == rhs.host && port == rhs.port;
    }

    bool operator!=(SocketAddr const& rhs) {
        return !(*this == rhs);
    }
};

std::ostream& operator<<(std::ostream& os, const SocketAddr& addr) {
    os <<
       ((addr.host >> 0 ) & 0xFF) << "." <<
       ((addr.host >> 8 ) & 0xFF) << "." <<
       ((addr.host >> 16) & 0xFF) << "." <<
       ((addr.host >> 24) & 0xFF) << " " <<
       addr.port;

    return os;
}

/// A packet of data with headers for optional delivery & order guarantees
template<class T>
struct Packet {
    SocketAddr addr {};
    GuaranteeLevel level = GuaranteeLevel::None;
    std::optional<Header> header {};
    std::optional<T> data {};

    inline bool is_guaranteed() const {
        return level == GuaranteeLevel::Delivery || level == GuaranteeLevel::Order;
    }

    inline Packet internal_respond(Header header) {
        Packet p {};
        p.addr = addr;
        p.level = level;
        p.header = header;
        return p;
    }
};

/// Encoding rule for guaranteed data T.
template<class Data, class DataRule>
struct packet_rule {
    DataRule data_rule {};

    void read(BitReader& r, Packet<Data>& packet) {
        packet = Packet<Data>{};
        bool has_data = true;

        guarantee_level_rule{}.read(r, packet.level);
        if (packet.is_guaranteed()) {
            Header _h {};
            Header::rule{}.read(r, _h);
            has_data = _h.kind == Header::Kind::Open || _h.kind == Header::Kind::Data;
            packet.header.emplace(_h);
        }

        if (has_data) {
            Data d{};
            data_rule.read(r, d);
            packet.data.emplace(d);
        }
    }

    void write(BitWriter& w, Packet<Data> const& packet) {
        guarantee_level_rule{}.write(w, packet.level);
        bool expect_data = true;
        if (packet.is_guaranteed()) {
            if (!packet.header.has_value()) {
                throw std::invalid_argument("packet_rule::write guaranteed packet has no header");
            }
            Header::rule{}.write(w, *packet.header);
            expect_data = packet.header->kind == Header::Kind::Open || packet.header->kind == Header::Kind::Data;
        }

        if (expect_data && !packet.data.has_value()) {
            throw std::invalid_argument("packet_rule::write expected data but data field is empty");
        }

        if (!expect_data && packet.data.has_value()) {
            throw std::invalid_argument("packet_rule::write unexpected data in ack packet");
        }

        if (packet.data.has_value()) {
            data_rule.write(w, *packet.data);
        }
    }
};

//! The state of a connection and the protocol implementation.

#pragma once

#include <map>
#include <set>
#include <iostream>
#include <optional>
#include <cstdint>
#include "xal_time.h"
#include "guarantee.hpp"
#include "seq.hpp"

/// The data associated with a single packet we're currently sending.
template <class Data>
struct SendData {
   /// The time we last attempted to send this message
   uint64_t last_send;
   /// The number of times we've tried to send this message
   int64_t attempts;
   /// The message data itself
   Packet<Data> message;
};

/// The status of a Connection
enum class Status {
    Sending,
    Idle,
    Expired,
};

/// The connection state associated with a single peer.
template <class Data>
struct Connection {
    SocketAddr remote_addr {};

    /// The ID of the receiving end of the connection
    std::optional<int32_t> rec_id {};
    /// The time we last received a message on this connection
    uint64_t last_rec;
    /// The sequence number of the most recent sequenced packet we received
    Seq rec_seq {};
    /// The most recently dispatched message
    Seq disp_seq {};
    /// A set of sequences which we have not received
    std::set<Seq> missing {};
    /// A buffer messages for which we're waiting on a missing packet before
    /// dispatching
    std::map<Seq, Packet<Data>> order_buf {};

    /// The time we last sent a packet
    uint64_t last_send;
    /// The ID of the sending part of this connection
    std::optional<int32_t> snd_id {};
    /// True if we've received an acknowledgement for our open request
    bool snd_open = false;
    /// A list of message for which we're waiting for an acknowledgement
    std::vector<std::pair<Seq, SendData<Data>>> snd_queue {};
    /// The sequence of the last message we send
    Seq snd_seq {};

    explicit Connection(uint64_t now) {
        last_rec = now;
        last_send = now;
    }

    /// Get the status of the connection. If the status is Idle, the connection
    /// doesn't have to be frequently updated, if it's expired, it should be
    /// deleted
    Status status(uint64_t now) {
        if (now - last_rec > seconds_to_ns(20) && now - last_send > seconds_to_ns(20)) {
            return Status::Expired;
        } else if (!snd_queue.empty()) {
            return Status::Sending;
        } else {
            // If we have acknowledgements for each of the packets we've sent,
            // we don't need to periodically re-send them, so the connection's
            // update method can be called less frequently
            return Status::Idle;
        }
    }

    /// Pass a received message to the connection for processing.
    /// Panics if the given message has no guarantee data.
    template<class Dispatch, class Send>
    void receive(
            Packet<Data> msg,
            uint64_t now,
            Dispatch dispatch,
            Send send
    ) {
        if (msg.header->kind == Header::Kind::Open) {
            handle_open(msg.header->open, msg, now, dispatch, send);
        }
        else if (msg.header->kind == Header::Kind::OpenAck) {
            handle_open_ack(msg.header->open_ack, now);
        }
        else if (msg.header->kind == Header::Kind::Data) {
            handle_data(msg.header->data, msg, now, dispatch, send);
        }
        else if (msg.header->kind == Header::Kind::DataAck) {
            auto seq_short = msg.header->data_ack;
            auto seq = expand(seq_short, snd_seq);
            // Try to remove the message
            for(auto it = snd_queue.begin(); it != snd_queue.end(); it++) {
                if (it->first.seq == seq.seq) {
                    snd_queue.erase(it);
                    break;
                }
            }
        }
    }
    /// Checks the messages currently being sent, and re-send them if needed.
    template<class Send>
    void update(uint64_t now, Send send) {
        // If we've already tried to send more than 30 times,
        // without an acknowledgement, remove the message.
        for (auto it = snd_queue.begin(); it != snd_queue.end();) {
            if (it->second.attempts > 30) {
                it = snd_queue.erase(it);
            } else {
                ++it;
            }
        }

        for (auto it = snd_queue.begin(); it != snd_queue.end(); it++) {
            auto seq = it->first;
            auto& m = it->second;
            // If it's been 400ms since we last tried to send, send it again.
            auto since_sent = now - m.last_send;
            if (since_sent > ms_to_ns(400.0)) {
                m.last_send = now;
                m.attempts += 1;
                //trace("Sending guaranteed data, seq: {:?}", seq);

                last_send = now;
                send(m.message);
            }
        }
    }

    /// Send the given message. The message's guarantee data is set, including
    /// a request to open a connection if needed, the message is added to the
    /// queue, and a reference to it returned to be sent through a socket.
    Packet<Data>& send(Packet<Data> msg, uint64_t now) {
        srand(now);
        snd_seq.seq += 1;
        auto seq = truncate(snd_seq);

        if (!snd_open) {
            if (!snd_id.has_value()) {
                // Generate a random ID, 0 is reserved
                do snd_id = { (uint32_t) rand() };
                while (*snd_id == 0);
            }
            msg.header = { Header{} };
            msg.header->kind = Header::Kind::Open;
            msg.header->open = OpenData{ *snd_id, rec_id.has_value(), seq };
        } else {
            msg.header = { Header{} };
            msg.header->kind = Header::Kind::Data;
            msg.header->data = seq;
        };

        last_send = now;
        auto data = SendData<Data> { now, 1, msg };
        snd_queue.push_back(std::make_pair<Seq, SendData<Data>>(Seq{snd_seq.seq}, std::move(data)));
        return snd_queue.back().second.message;
    }

    /// Handle a request to open a connection
    template<class Dispatch, class Send>
    void handle_open(
            OpenData data,
            Packet<Data> msg,
            uint64_t now,
            Dispatch dispatch,
            Send send
    ) {
        if (rec_id.has_value() && data.id != *rec_id) reset_rec(now);
        if (snd_open && !data.is_open) reset_snd(now);

        // Set our ID and acknowledge the open
        rec_id = { data.id };
        auto ack = Header {};
        ack.kind = Header::Kind::OpenAck;
        ack.open_ack = data.id;
        send(msg.internal_respond(ack));

        // Receive the data appended into the open message
        msg.header = { Header{} };
        msg.header->kind = Header::Kind::Data;
        msg.header->data = data.seq;

        receive(msg, now, dispatch, send);
    }

    void handle_open_ack(int32_t id, uint64_t now) {
        if (id == 0) {
            // An ID of `0` is used as a request to reset the connection.
            reset_snd(now);
        } else if (!snd_id.has_value()) {
            std::cerr << "Got openack for unrequested connection\n";
        } else {
            if (id == *snd_id) {
                snd_open = true;
            } else {
                std::cerr << "Got openack with unmatching ID\n";
                reset_snd(now);
            }
        }
    }

    /// Reset the receiving end of the connection
    void reset_rec(uint64_t now) {
        rec_id = {};
        last_rec = now;
        rec_seq = Seq{0};
        disp_seq = Seq{0};
        missing.clear();
        order_buf.clear();
    }

    /// Reset the sending end of the connection
    void reset_snd(uint64_t now) {
        last_send = now;
        snd_id = {};
        snd_open = false;
        snd_seq = Seq{0};

        // Attempt to resend any queued messages
        auto old_queue = std::vector<std::pair<Seq, SendData<Data>>>{};
        std::swap(snd_queue, old_queue);
        for (auto& m : old_queue) {
            send(m.second.message, now);
        }
    }

    template<class Dispatch, class Send>
    void handle_data(
        SeqSmall seq,
        Packet<Data> msg,
        uint64_t now,
        Dispatch dispatch,
        Send send
    ) {
        if (!rec_id.has_value()) {
            // Senders should send open requests with every message until
            // we acknowledge it -- if we get data without having opened
            // a connection, something's wrong -- we request that the sender
            // reset their connection, sending a new open request.
            std::cerr << "Got data without a connection, requesting that"
                         "the sender re-open the connection.\n";

            auto reset_req = Header {};
            reset_req.kind = Header::Kind::OpenAck;
            reset_req.open_ack = 0;
            send(msg.internal_respond(reset_req));

            // When they reset their sender we need to start receiving at
            // seq 0 again, so we reset our receiver.
            reset_rec(now);
            return;
        }

        // Calculate the full sequence number from the truncated data.
        auto seq_long = expand(seq, rec_seq);

        auto prev_rec = last_rec;
        last_rec = now;

        // Acknowledge the packet.
        auto ack = Header {};
        ack.kind = Header::Kind::DataAck;
        ack.data_ack = seq;
        send(msg.internal_respond(ack));

        auto is_new = seq_long.seq > rec_seq.seq;

        auto missing_it = missing.find(seq_long);
        auto was_missing = missing_it != missing.end();
        if (was_missing) missing.erase(missing_it);

        if (is_new) {
            // Check for sequence numbers we haven't seen yet.
            for (auto i = rec_seq.seq + 1; i < seq_long.seq; i++) {
                missing.insert(Seq { i });
            }
            rec_seq = seq_long;
        }

        if (is_new || was_missing) {
            if (msg.level == GuaranteeLevel::Delivery) {
                // If the sender doesn't care about the message order, we
                // dispatch it.
                dispatch(msg);
            } else {
                // Otherwise, we might have to buffer it, or send currently
                // buffered messages
                auto waiting = now - prev_rec;

                // If this message is the next in the sequence, dispatch it,
                // otherwise add it to the buffer
                if (seq_long.seq == disp_seq.seq + 1) {
                    dispatch(msg);
                    disp_seq = seq_long;
                } else {
                    order_buf.emplace(seq_long, msg);
                }

                // Loop through the order buffer, dispatching in sequence
                // as much data as we have
                while(true) {
                    auto next = Seq { disp_seq.seq + 1 };
                    auto it = order_buf.find(next);
                    if (it != order_buf.end()) {
                        dispatch(it->second);
                        order_buf.erase(it);
                        disp_seq = next;
                    } else {
                        break;
                    }
                }
            }
        }
    }
};

#pragma once

#include <memory>
#include <map>
#include <thread>
#include "zed_net.h"
#include "connection.hpp"
#include "xal_time.h"
#include "channel.hpp"
#ifdef _WIN32
#include <winsock.h>
#else
#include <sys/socket.h>
#endif

using UdpSocket = std::shared_ptr<zed_net_socket_t>;

/// A single-producer-single-consumer channel.
template<class Data>
using Channel = std::shared_ptr<spsc_bounded_queue_t<Data>>;

/// Take all the incoming messages from a channel
template<class T, class F>
void channel_recv(Channel<T> &channel, F fn) {
    std::optional<T> packet {};
    // Forward raw messages to the connection manager.
    while ((packet = channel->dequeue()).has_value()) {
        auto msg = std::move(packet.value());
        packet = {};
        fn(std::move(msg));
    }
}

/// Encode a message with the given rule
template<class T, class R>
std::vector<uint8_t> encode(R rule, T const& value) {
    auto w = BitWriter{};
    rule.write(w, value);
    return w.data;
}

/// Decode a message with the given rule
template<class T, class R>
T decode(R rule, const std::vector<uint8_t> data) {
    auto r = BitReader{ data };
    auto val = T{};
    rule.read(r, val);
    return std::move(val);
}

/// Manages a set of connections
template<class Data, class Rule>
struct Connections {
    UdpSocket socket = nullptr;
    Channel<Packet<Data>> incoming = nullptr;
    std::vector<Connection<Data>> connections {};
    Rule codec {};

    void raw_send(Packet<Data> msg) {
        auto data = encode(codec, msg);
        if (zed_net_udp_socket_send(&*socket, msg.addr, data.data(), (int) data.size()) == -1) {
#ifdef _WIN32
            auto err = WSAGetLastError();
#else
            auto err = errno;
#endif
            std::cerr << "Failed to send to " << msg.addr << " on socket (errno " << err << "): " << zed_net_get_error() << "\n";
        }
    }

    /// Handle an incoming message
    void recv(Packet<Data> msg) {
        // Check if the message is guaranteed, if it is, send pass it
        // through to this client's `Connection`, otherwise add it
        // to the dispatch queue
        if (msg.is_guaranteed()) {
            auto send = [&](Packet<Data> msg) { raw_send(std::move(msg)); };
            auto dispatch = [&](Packet<Data> msg) { incoming->enqueue(std::move(msg)); };

            connection(msg).receive(msg, now(), dispatch, send);
        } else {
            incoming->enqueue(std::move(msg));
        }
    }

    /// Update the connections, resending messages with missing acknowledgements.
    void update() {
        auto time = now();

        // Remove expired connections
        for (auto it = connections.begin(); it != connections.end();) {
            if (it->status(time) == Status::Expired) {
                it = connections.erase(it);
            } else ++it;
        }

        // Update all the connections, re-sending any messages given to us by
        // each connection
        auto send = [&](Packet<Data> msg) { raw_send(msg); };
        for (auto& c : connections) c.update(time, send);
    }

    /// Send a message on the socket
    void send(Packet<Data> msg) {
        if (msg.is_guaranteed()) msg = connection(msg).send(msg, now());
        raw_send(msg);
    }

    /// Get the connection for the given message
    Connection<Data>& connection(Packet<Data> const& msg) {
        for (auto& c : connections) {
            if (c.remote_addr.host == msg.addr.host && c.remote_addr.port == msg.addr.port) return c;
        }
        connections.push_back(Connection<Data>(now()));
        connections.back().remote_addr = msg.addr;
        return connections.back();
    }
};


/// Wraps a UDP socket with a connection protocol for optionally guaranteeing
/// the delivery and order of messages on a per-message basis.
/// The interface is based on channels and relies on an external loop to regularly
/// call the update method and forward raw incoming messages to the connection manager.
template<class Data, class Rule>
struct Socket {
    /// Manages connections.
    /// Must be updated regularly (every ~50ms).
    /// To send a message over the socket, use connections.send(..).
    Connections<Data, packet_rule<Data, Rule>> connections;

    /// Receiver for message data.
    Channel<Packet<Data>> incoming_rx;

    /// Messages received over the socket, must be passed to connections, which forwards the actual
    /// message data to the `incoming` receiver.
    Channel<Packet<Data>> raw_rx;

    /// A handle to the thread listening for incoming messages
    std::thread listen_thread;

    /// Construct a Socket given the port to bind to and a rule for reading & writing messages
    static Socket bind(uint16_t port, Rule codec) {
        if (zed_net_init()) {
            std::cerr << zed_net_get_error() << "\n";
            exit(1);
        }

        std::cout << "Opening socket (port " << port << ")" << std::endl;
        auto socket = std::make_shared<zed_net_socket_t>();
        if (zed_net_udp_socket_open(&*socket, port, 0)) {
            std::cerr << zed_net_get_error() << "\n";
            exit(1);
        }

        auto timeout = 2000;
        if (setsockopt(socket->handle, SOL_SOCKET, SO_RCVTIMEO, (char*)&timeout, sizeof(timeout)))

        auto rule = packet_rule<Data, Rule>{ codec };

        auto incoming = std::make_shared<spsc_bounded_queue_t<Packet<Data>>>(64);
        auto raw = std::make_shared<spsc_bounded_queue_t<Packet<Data>>>(64);

        // Spawn a thread to listen for incoming messages & forward to the channel.
        auto thread = std::thread([=]() {
            auto rule = packet_rule<Data, Rule>{ codec };
            auto buffer = std::vector<uint8_t>(2048, 0);
            bool b = true;
            while(b) {
                SocketAddr sender;
                auto bytes_read = zed_net_udp_socket_receive(&*socket, &sender, buffer.data(), (int) buffer.size());
                if (errno != 0 && errno != EAGAIN && errno != EWOULDBLOCK) {
                    std::cerr << "recvfrom failed: " << errno << "\n";
                    exit(1);
                }
                if (bytes_read > 0) {
                    try {
                        buffer.resize((size_t) bytes_read);
                        auto r = BitReader::open(buffer);
                        Packet<Data> msg;
                        rule.read(r, msg);
                        msg.addr = sender;
                        raw->enqueue(std::move(msg));
                        buffer.resize(2048, 0);
                    } catch (std::exception &e) {
                        std::cerr << "Failed to decode message: " << e.what() << "\n";
                    }
                }
            }
        });

        thread.detach(); // TODO: not this..

        return Socket { { socket, incoming, {}, {} }, incoming, raw, std::move(thread) };
    }
};


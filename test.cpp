#include <string>
#include "encode.hpp"
#include "seq.hpp"
#include "guarantee.hpp"
#include "connection.hpp"
#include "socket.hpp"
#include "sequencer.hpp"

template <class T, class R>
void cycle(T value, R rule) {
    BitWriter w{};
    rule.write(w, value);
    auto r = BitReader::open(w.data);
    T cycled;
    rule.read(r, cycled);
    assert(value == cycled);
};

struct MyStruct {
    int a;
    float b;
};

using my_struct_rule = all_of<
    field_rule<&MyStruct::a, int_rule<0, 10>>,
    field_rule<&MyStruct::b, int_rule<0, 20>>
>;

struct MyVariant {
    enum class Kind { A, B, C, Count } kind;
    using kind_rule = enum_rule<(uint64_t) Kind::Count>;

    union {
        int a;
        float b;
        MyStruct c;
    };
};

using my_variant_rule = variant_rule<
    field_rule<&MyVariant::kind, MyVariant::kind_rule>,
    field_rule<&MyVariant::a, int_rule<0, 10>>,
    // TODO: MSVC fails if two fields have the same rule, works with gcc, compiler bug?
    field_rule<&MyVariant::b, int_rule<0, 20>>,
    field_rule<&MyVariant::c, my_struct_rule>
>;

void test_seq_truncate() {
    assert(0x0    == truncate(Seq{0x0}).seq);
    assert(0x1    == truncate(Seq{0x1}).seq);
    assert(0xFFFF == truncate(Seq{0xFFFF}).seq);
    assert(0x0    == truncate(Seq{0x10000}).seq);
    assert(0xFFFF == truncate(Seq{0x1FFFF}).seq);
}

void test_expand() {
    assert(0x0     == expand(SeqSmall{0x0}, Seq{0x0}).seq);
    assert(0x0     == expand(SeqSmall{0x0}, Seq{0x7FFF}).seq);
    assert(0x10000 == expand(SeqSmall{0x0}, Seq{0x8000}).seq);

    assert(0x1     == expand(SeqSmall{0x1}, Seq{0x0}).seq);
    assert(0x1     == expand(SeqSmall{0x1}, Seq{0x8000}).seq);
    assert(0x10001 == expand(SeqSmall{0x1}, Seq{0x8001}).seq);
}

void test_int_rule() {
    auto rule = int_rule<0, 10, 10>{};
    cycle(0.3f, rule);
}

void test_variant_rule() {
    BitWriter w{};
    auto v = MyVariant{MyVariant::Kind::B};
    v.b = 4;
    my_variant_rule{}.write(w, v);

    auto r = BitReader::open(w.data);
    MyVariant test{};
    my_variant_rule{}.read(r, test);
    assert(test.kind == MyVariant::Kind::B);
    assert(test.b == 4);
}

void test_struct_rule() {
    BitWriter w{};
    auto s = MyStruct { 1, 2 };
    my_struct_rule{}.write(w, s);

    auto r = BitReader::open(w.data);
    MyStruct test;
    my_struct_rule{}.read(r, test);
    assert(test.a == 1);
    assert(test.b == 2);
}

void test_optional_rule() {
    {
        BitWriter w{};
        std::optional<int> none = {};
        using rule = option_rule<int_rule<0, 10>>;
        rule{}.write(w, none);

        auto r = BitReader::open(w.data);
        std::optional<int> maybe;
        rule{}.read(r, maybe);
        assert(!maybe.has_value());
    }

    {
        BitWriter w{};
        std::optional<int> some = {5};
        using rule = option_rule<int_rule<0, 10>>;
        rule{}.write(w, some);

        auto r = BitReader::open(w.data);
        std::optional<int> maybe;
        rule{}.read(r, maybe);
        assert(maybe.has_value());
        assert(*maybe == 5);
    }
}

void run_server() {
    auto socket = Socket<std::string, string_rule>::bind(27288, string_rule{});

    std::cout << "Starting server" << std::endl;

    bool end = false;
    while(!end) {
        socket.connections.update();

        std::optional<Packet<std::string>> packet {};
        // Forward raw messages to the connection manager.
        while ((packet = socket.raw_rx->dequeue()).has_value()) {
            auto msg = std::move(packet.value());
            packet = {};
            std::cout << "Server got raw msg" << std::endl;
            socket.connections.recv(msg);

            if (msg.header->kind == Header::Kind::DataAck) {
                end = true;
            }
        }

        // Take incoming messages
        while ((packet = socket.incoming_rx->dequeue()).has_value()) {
            auto msg = std::move(packet.value());
            packet = {};
            std::cout << "Server got a message, echoing: " << *msg.data << std::endl;
            socket.connections.send(msg);
        }
    }

}

void run_client() {
    auto socket = Socket<std::string, string_rule>::bind(27289, string_rule{});

    auto msg = Packet<std::string>{};
    zed_net_get_address(&msg.addr, "localhost", 27288);
    msg.data = { std::string("Hello world o/") };
    msg.level = GuaranteeLevel::Order;

    std::cout << "Client sending a message" << std::endl;
    socket.connections.send(msg);

    while(true) {
        socket.connections.update();

        std::optional<Packet<std::string>> packet {};
        // Forward raw messages to the connection manager.
        while ((packet = socket.raw_rx->dequeue()).has_value()) {
            auto msg = std::move(packet.value());
            packet = {};
            socket.connections.recv(msg);
        }

        // Take incoming messages
        while ((packet = socket.incoming_rx->dequeue()).has_value()) {
            auto msg = std::move(packet.value());
            packet = {};
            std::cout << "Client got a message: " << *msg.data << std::endl;
            return;
        }
    }
}

void test_connection() {
    auto server_thread = std::thread([](){ run_server(); });
    std::this_thread::sleep_for(std::chrono::duration(std::chrono::milliseconds(1000)));
    auto client_thread = std::thread([](){ run_client(); });
    server_thread.join();
    client_thread.join();
};

void test_sequencer() {
    auto rule = bits_rule<uint16_t, 16>{};

    Sequencer<uint16_t, uint16_t> seq_a{};
    Sequencer<uint16_t, uint16_t> seq_b{};

    for (int i = 0; i < 50'000; i++) {
        seq_a.add_event(rule, i);
        auto msg_a = seq_a.make_msg();

        seq_b.receive(msg_a);
        auto ev = seq_b.next_ev(rule);
        assert(ev.has_value());
        assert(*ev == i);
    }
}

int main() {
    test_expand();
    test_seq_truncate();
    test_int_rule();
    test_variant_rule();
    test_struct_rule();
    test_optional_rule();
    test_connection();
    test_sequencer();
}

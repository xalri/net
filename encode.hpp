/// A library for expressing complex bit-packed encodings using a small set of
/// primitive encodings and a set of 'combinators' which combine them in various ways.

#pragma once

#include <cstddef>
#include <cstdint>
#include <cassert>
#include <algorithm>
#include <tuple>
#include <stdexcept>
#include <vector>
#include <optional>
#include <cstring>
#include "tuple_util.hpp"

/// A position in a bit-stream.
struct Cursor {
    size_t byte = 0;
    size_t bit_offset = 0;

    /// Get the position in bits
    size_t pos() const {
        return (byte * 8) + bit_offset;
    }
};

/// A bit-wise read operation through some raw data.
struct BitReader {
    std::vector<std::uint8_t> const& data;
    Cursor cursor{};

    /// Open a reader for some data.
    static BitReader open(std::vector<std::uint8_t> const& data);

    /// Read a raw value of a specified bit-width.
    /// If we have reached the end of the data, throws an exception.
    uint64_t read_raw(uint64_t size);

    /// Seek to a position in bits.
    inline void seek(size_t pos) {
        cursor.byte = pos / 8;
        cursor.bit_offset = pos % 8;
    }

    /// Get the number of bits we've traversed so far.
    inline size_t pos() const {
        return cursor.pos();
    }

    /// Get the total length in bytes of the vector we're reading.
    inline size_t size() const {
        return data.size();
    }

    /// Returns true if there is no data in the reader
    inline bool is_empty() const {
        return size() == 0;
    }

    /// Determine whether the reader is in a possible terminal state, in which the useful data has
    /// already been read. Returns false <=> there are no more bytes to read, and all bits that
    /// remain in the current byte are zero.
    inline bool is_terminal() const {
        return cursor.byte == data.size() ||
               (cursor.byte == data.size() - 1 && (uint64_t) data[cursor.byte] < (2 ^ (cursor.bit_offset + 2)));
    }
};

struct BitWriter {
    Cursor cursor{};
    std::vector<std::uint8_t> data {};

    /// Write a raw value of a specified bit-width.
    void write_raw(uint64_t size, uint64_t v);

    /// Get the number of bits we've traversed so far.
    inline size_t pos() const {
        return cursor.pos();
    }

    /// Get the total length in bytes of the vector we're writing.
    inline size_t size() const {
        return data.size();
    }

    /// Returns true if there is no data in the writer
    inline bool is_empty() const {
        return size() == 0;
    }
};


// Primitives:

/// Encoding rule for an `N` bit unsigned integer.
template <class T, uint64_t N>
struct bits_rule {
    static_assert(N <= 64, "invalid bits_rule");

    void read(BitReader& r, T& val) const {
        val = static_cast<T>(r.read_raw(N));
    }

    void write(BitWriter& w, T const& val) const {
        w.write_raw(N, static_cast<uint64_t>(val));
    }
};

/// Encoding for `bool`s using a single bit.
using bool_rule = bits_rule<bool, 1>;

using i8_rule  = bits_rule<int8_t, 8>;
using i16_rule = bits_rule<int16_t, 16>;
using i32_rule = bits_rule<int32_t, 32>;
using i64_rule = bits_rule<int64_t, 64>;

using u8_rule  = bits_rule<uint8_t, 8>;
using u16_rule = bits_rule<uint16_t, 16>;
using u32_rule = bits_rule<uint32_t, 32>;
using u64_rule = bits_rule<uint64_t, 64>;

/// Count the leading zeros of a value
constexpr uint64_t clz(uint64_t x) noexcept {
    if (x == 0) return 64;
    auto n = 0u;
    for (auto i = 1u; i < 64; i++) {
        if (x & 0x8000'0000'0000'0000ull) break;
        n++;
        x = x << 1;
    }
    return n;
}

/// Get the number of bits required to hold `x` values.
constexpr uint64_t n_bits(uint64_t x) noexcept {
    if (x == 0) return 1;
    return 64 - clz(x - 1);
}

/// Rule for encoding enum classes
template <uint64_t max>
struct enum_rule {
    static const uint64_t bits = n_bits((uint64_t)(max + 1));
    static constexpr auto inner_rule = bits_rule<uint64_t, bits>{};

    void range_check(uint64_t val) const {
        if (val > max)
            throw std::invalid_argument("enum_rule out of range");
    }

    template<class T>
    void read(BitReader& r, T& val) const {
        uint64_t i;
        inner_rule.read(r, i);
        range_check(i);
        val = (T) i;
    }

    template<class T>
    void write(BitWriter& w, T const& val) const {
        range_check((uint64_t) val);
        inner_rule.write(w, (uint64_t)(val));
    }
};

/// Rule for encoding numbers as signed scaled integers.
template <int64_t _min, int64_t _max, int64_t scale = 1>
struct int_rule {
    static_assert(_min < _max, "int_rule minimum must be smaller than maximum");
    static_assert(scale >= 1, "int_rule scale must not be zero");

    static const int64_t min = _min * scale;
    static const int64_t max = _max * scale;
    static const uint64_t bits = n_bits((uint64_t)(max - min + 1));
    static constexpr auto inner_rule = bits_rule<uint64_t, bits>{};

    void range_check(int64_t val) const {
        if (val < min || val > max)
            throw std::invalid_argument("int_rule out of range");
    }

    template<class T>
    void read(BitReader& r, T& val) const {
        uint64_t i;
        inner_rule.read(r, i);
        int64_t j = i + min;
        range_check(j);
        val = (T) j / (T) scale;
    }

    template<class T>
    void write(BitWriter& w, T const& val) const {
        auto v = (int64_t)(val * (T) scale);
        range_check(v);
        inner_rule.write(w, (uint64_t)(v - min));
    }
};


// Combinators:

/// Rule for encoding a single field of a struct
/// e.g. `field_rule<&MyStruct::field_name, some_rule>{}`
template<auto Offset, class Rule> struct field_rule {
    template<class Struct, class Field, Field Struct::*O, class R>
    static Struct struct_helper(const field_rule<O, R> *) { };

    using Struct = decltype(struct_helper(static_cast<field_rule<Offset, Rule>*>(nullptr)));

    template<class Struct, class Field, Field Struct::*O, class R>
    static Field field_helper(const field_rule<O, R> *) { };

    using Field = decltype(field_helper(static_cast<field_rule<Offset, Rule>*>(nullptr)));

    Rule inner {};

    Field& field_ref(Struct &s) const { return s.*Offset; }
    Field const& field_ref(Struct const& s) const { return s.*Offset; }

    void read(BitReader& r, Struct& val) const {
        inner.read(r, field_ref(val));
    }

    void write(BitWriter& w, Struct const& val) const {
        inner.write(w, field_ref(val));
    }
};

struct string_rule {
    void read(BitReader& r, std::string& v) const {
        uint16_t len;
        bits_rule<uint16_t, 16>{}.read(r, len);
        v = std::string((size_t) len, ' ');
        for (char &c : v) bits_rule<char, 8>{}.read(r, c);
    }

    void write(BitWriter& w, std::string const& v) const {
        bits_rule<uint16_t, 16>{}.write(w, (uint16_t) v.length());
        for (char const& c : v) bits_rule<char, 8>{}.write(w, c);
    }
};

struct NestedData {
    /// The exact length in bits
    size_t len = 0;
    /// The actual data
    std::vector<uint8_t> data{};

    /// Create a `NestedData` from a bitwriter.
    static NestedData from_writer(BitWriter w) {
        return { (size_t) w.pos(), w.data };
    }

    /// Get a BitReader to read the data.
    BitReader into_reader() & {  // reference qualified, can't be called on temporaries.
        return BitReader::open(data);
    }

    struct rule {
        void read(BitReader& r, NestedData & v) const {
            v.data.clear();
            bits_rule<size_t, 16>{}.read(r, v.len);
            for(auto i = 0u; i < (v.len + 7) / 8; i++) {
                v.data.push_back(0);
                bits_rule<uint8_t, 8>{}.read(r, v.data.back());
            }
        }

        void write(BitWriter& w, NestedData const& v) const {
            bits_rule<size_t, 16>{}.write(w, v.len);
            for (uint8_t const& c : v.data) bits_rule<uint8_t , 8>{}.write(w, c);
        }
    };
};

template<class ElemRule, size_t len_bits = 16>
struct vec_rule {
    ElemRule elem_rule{};

    using len_rule = bits_rule<size_t, len_bits>;
    const static size_t max_elems = (1 << len_bits) - 1;

    template<class T>
    void read(BitReader& r, std::vector<T>& v) const {
        size_t len;
        len_rule{}.read(r, len);
        v = std::vector<T>{};
        for(auto i = 0u; i < len; i++) {
            T elem;
            elem_rule.read(r, elem);
            v.push_back(elem);
        }
    }

    template<class T>
    void write(BitWriter& w, std::vector<T> const& v) const {
        if (v.size() > max_elems) {
            throw std::invalid_argument("vec_rule write vector is too large!");
        }

        len_rule{}.write(w, v.size());
        for (auto const& e : v) elem_rule.write(w, e);
    }

};

/// An encoding which does nothing.
struct empty_rule {
    template<class Struct>
    void read(BitReader& _r, Struct& _val) const { }

    template<class Struct>
    void write(BitWriter& _w, Struct const& _val) const { }
};

/// Combines a sequence of rules
template<class... Rules>
struct all_of {
    std::tuple<Rules...> rules {};

    template<class Struct>
    void read(BitReader& r, Struct& val) const {
        for_each_in_tuple(rules, [&](auto rule) { rule.read(r, val); });
    }

    template<class Struct>
    void write(BitWriter& w, Struct const& val) const {
        for_each_in_tuple(rules, [&](auto rule) { rule.write(w, val); });
    }
};

template<class Rule>
struct option_rule {
    Rule rule{};

    template<class Struct>
    void read(BitReader& r, std::optional<Struct>& val) const {
        bool has_value;
        bool_rule{}.read(r, has_value);
        if (has_value) {
            val.emplace();
            rule.read(r, *val);
        }
    }

    template<class Struct>
    void write(BitWriter& w, std::optional<Struct> const& val) const {
        bool_rule{}.write(w, val.has_value());
        if (val.has_value()) {
            rule.write(w, *val);
        }
    }
};

/// A combinator for encoding tagged enums.
/// The first parameter is a field rule for encoding the tag, the following parameters
/// are field rules for elements of the union in order of the tag values starting at 0.
template<class Tag, class... Rules>
struct variant_rule {
    Tag tag{};
    std::tuple<Rules...> rules{};

    template<class Struct>
    void read(BitReader& r, Struct& val) const {
        tag.read(r, val);
        auto t = (int) tag.field_ref(val);
        if (t >= std::tuple_size<decltype(rules)>())
            throw std::runtime_error("one_of read invalid tag");

        auto f = [&](auto const& rule) { rule.read(r, val); };
        apply(rules, t, f);
    }

    template<class Struct>
    void write(BitWriter& w, Struct const& val) const {
        tag.write(w, val);
        auto t = (int) tag.field_ref(val);

        auto f = [&](auto const& rule) { rule.write(w, val); };
        apply(rules, t, f);
    }
};

#pragma once

#include <cstdint>
#include "encode.hpp"

struct Seq {
    int64_t seq = 0;

    bool operator==(const Seq &rhs) const {
        return seq == rhs.seq;
    }

    bool operator!=(const Seq &rhs) const {
        return !(rhs == *this);
    }

    bool operator<(const Seq &rhs) const {
        return seq < rhs.seq;
    }

    bool operator>(const Seq &rhs) const {
        return rhs < *this;
    }

    bool operator<=(const Seq &rhs) const {
        return !(rhs < *this);
    }

    bool operator>=(const Seq &rhs) const {
        return !(*this < rhs);
    }
};

struct SeqSmall {
    uint16_t seq = 0;
    using rule = field_rule<&SeqSmall::seq, bits_rule<uint16_t, 16>>;
};

/// Expand a truncated sequence number to a full sequence number
Seq expand(SeqSmall small, Seq prev) {
    auto seq = (uint32_t) small.seq;
    auto a = 1 << 16;
    while (seq < prev.seq) seq += a;
    if (seq > prev.seq + a / 2) seq -= a;
    return Seq{ seq };
}

SeqSmall truncate(Seq s) {
    return SeqSmall{ (uint16_t) s.seq };
}
